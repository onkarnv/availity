package com.NewVision.InnoVision.engine.customMethods;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.python.antlr.PythonParser.try_stmt_return;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.NewVision.InnoVision.engine.commands.General;
import com.NewVision.InnoVision.engine.commands.galenCommands.Report;
import com.NewVision.InnoVision.engine.core.CommandControl;
import com.NewVision.InnoVision.engine.core.Control;
import com.NewVision.InnoVision.engine.execution.exception.element.ElementException;
import com.NewVision.InnoVision.engine.execution.exception.element.ElementException.ExceptionType;
import com.NewVision.InnoVision.engine.support.Status;
import com.NewVision.InnoVision.engine.support.methodInf.Action;
import com.NewVision.InnoVision.engine.support.methodInf.InputType;
import com.NewVision.InnoVision.engine.support.methodInf.ObjectType;
import java.io.File;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Custom extends General {

    public Custom(CommandControl cc) {
		super(cc);
	}
    

@Action(object = ObjectType.BROWSER, desc = "Print Input and Condition",input=InputType.YES,condition=InputType.YES)
public void PrintInputCondition() {
    System.out.println("Input is "+Input);
    System.out.println("Input is "+Condition);
    System.out.println("Data is " + Data);
    Report.updateTestLog(Action, "["+Input+"]"+ ",["+Data+"]"+ ",["+Condition+"]", Status.DONE);
}

@Action(object = ObjectType.BROWSER, desc = "Returns Absolute Path %absPath%",input=InputType.NO,condition=InputType.NO)
public void returnAbsolutePath() {
    String projectLocation = Control.getCurrentProject().getLocation();
    String absolutePath = projectLocation + "/Uploads/" ;
    addVar("%absPath%",absolutePath);
    Report.updateTestLog(Action, "Path :["+absolutePath+"] generated", Status.DONE);

}

@Action(object = ObjectType.BROWSER, desc = "store  value [<Data>] in Variable [<Condition>]", input = InputType.YES, condition = InputType.YES)
public void updateUserDefine() {
    if (Condition.startsWith("%") && Condition.endsWith("%")) {
       	addGlobalVar(Condition, Data);
        Report.updateTestLog(Action, "Value" + Data
                + "' is stored in Variable '" + Condition + "'",
                Status.DONE);
    } else {
        Report.updateTestLog(Action, "Variable format is not correct", Status.DEBUG);
    }
}

@Action(object = ObjectType.BROWSER, desc = "fn before storeDataFromPreviousTestCase AddVar input<@(TCName)>Condition<%testCase%>,In fn input<DestinationSheet:DestnColumn> condition<fromSheetName:FromsheetColoum>", input = InputType.NO, condition = InputType.YES)
public void ClearData() {
	String projectLocation = Control.getCurrentProject().getLocation();
	String D = projectLocation + "/TestData/" + Condition;
	String S = projectLocation + "/Backup/" + Condition;
	try {
		try {
			
            // Define the source and destination files
            File sourceFile = new File(S);
            File destFile = new File(D);

            // Use the Files.copy() method to overwrite the file contents
            Path sourcePath = Paths.get(sourceFile.getAbsolutePath());
            Path destPath = Paths.get(destFile.getAbsolutePath());
            Files.copy(sourcePath, destPath ,StandardCopyOption.REPLACE_EXISTING);

            // Print a message to indicate that the file has been overwritten
            System.out.println("CSV file has been overwritten with the contents of: " + sourceFile.getAbsolutePath());
            Report.updateTestLog(Action, "Value : [" +  S   + "] is sucessfully stored", Status.DONE);
        } catch (Exception ex) {
            ex.printStackTrace();
            Report.updateTestLog(Action, "D : [" +  D  + "] is sucessfully stored", Status.DONE);
        } 
		//Report.updateTestLog(Action, "Value : [" + D + "] is sucessfully stored", Status.DONE);

	} catch (Exception e) {
		e.printStackTrace();
	}
}

@Action(object = ObjectType.BROWSER, desc = "update the NPI in the sheet input<NPI to be update>condition<fileName.xlxs>",input=InputType.YES,condition=InputType.YES)
public void ClearExcel() {
	String projectLocation = Control.getCurrentProject().getLocation();
	String absolutePath = projectLocation + "/Uploads/" + Condition;
	try {
		FileInputStream fis = new FileInputStream(absolutePath);
		try {
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet = wb.getSheetAt(0);
			int row = Integer.parseInt(Data.split(",")[0]);
			int col = Integer.parseInt(Data.split(",")[1]);
			// DataFormatter formatter = new DataFormatter();
			sheet = wb.getSheetAt(0);
			Cell cell2Update = sheet.getRow(row).getCell(col);// update A2 cell in excel
			cell2Update.setBlank();// 
			
			FileOutputStream outputStream = new FileOutputStream(absolutePath);
			wb.write(outputStream);
			wb.close();
			outputStream.close();

			Report.updateTestLog(Action, "Path :[" + absolutePath + " + "+row+" + "+col+" ] generated", Status.DONE);
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.OFF, e.getMessage(), e);
			Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
		}
	} catch (Exception e) {
		Logger.getLogger(this.getClass().getName()).log(Level.OFF, e.getMessage(), e);
		Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
	}

}

@Action(object = ObjectType.BROWSER, desc = "update the D1 in the sheet input<NPI to be update>condition<fileName.xlxs>",input=InputType.YES,condition=InputType.YES)
public void updateDate1Excel() {
	String projectLocation = Control.getCurrentProject().getLocation();
	String absolutePath = projectLocation + "/Uploads/" + Condition;
	try {
		FileInputStream fis = new FileInputStream(absolutePath);
		try {
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet = wb.getSheetAt(0);
			// DataFormatter formatter = new DataFormatter();
			sheet = wb.getSheetAt(0);
						
			Cell cellD1Update = sheet.getRow(0).getCell(3);// update D1 cell in excel
			cellD1Update.setCellValue(Data);// Update Date
			
			FileOutputStream outputStream = new FileOutputStream(absolutePath);
			wb.write(outputStream);
			wb.close();
			outputStream.close();

			Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.OFF, e.getMessage(), e);
			Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
		}
	} catch (Exception e) {
		Logger.getLogger(this.getClass().getName()).log(Level.OFF, e.getMessage(), e);
		Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
	}

}
@Action(object = ObjectType.BROWSER, desc = "update the D2 in the sheet input<NPI to be update>condition<fileName.xlxs>",input=InputType.YES,condition=InputType.YES)
public void updateDate2Excel() {
	String projectLocation = Control.getCurrentProject().getLocation();
	String absolutePath = projectLocation + "/Uploads/" + Condition;
	try {
		FileInputStream fis = new FileInputStream(absolutePath);
		try {
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet = wb.getSheetAt(0);
			// DataFormatter formatter = new DataFormatter();
			sheet = wb.getSheetAt(0);
			
			Cell cellD2Update = sheet.getRow(0).getCell(4);// update E1 cell in excel
			cellD2Update.setCellValue(Data);// Update Date
			
			FileOutputStream outputStream = new FileOutputStream(absolutePath);
			wb.write(outputStream);
			wb.close();
			outputStream.close();

			Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.OFF, e.getMessage(), e);
			Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
		}
	} catch (Exception e) {
		Logger.getLogger(this.getClass().getName()).log(Level.OFF, e.getMessage(), e);
		Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
	}

}
@Action(object = ObjectType.BROWSER, desc = "update the D3 in the sheet input<NPI to be update>condition<fileName.xlxs>",input=InputType.YES,condition=InputType.YES)
public void updateDate3Excel() {
	String projectLocation = Control.getCurrentProject().getLocation();
	String absolutePath = projectLocation + "/Uploads/" + Condition;
	try {
		FileInputStream fis = new FileInputStream(absolutePath);
		try {
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet = wb.getSheetAt(0);
			// DataFormatter formatter = new DataFormatter();
			sheet = wb.getSheetAt(0);
			
			Cell cellD3Update = sheet.getRow(0).getCell(5);// update F1 cell in excel
			cellD3Update.setCellValue(Data);// Update Date

			FileOutputStream outputStream = new FileOutputStream(absolutePath);
			wb.write(outputStream);
			wb.close();
			outputStream.close();

			Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.OFF, e.getMessage(), e);
			Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
		}
	} catch (Exception e) {
		Logger.getLogger(this.getClass().getName()).log(Level.OFF, e.getMessage(), e);
		Report.updateTestLog(Action, "Path :[" + absolutePath + "] generated", Status.DONE);
	}

}

@Action(object = ObjectType.SELENIUM, desc = "Enter the value [<Data>] in the Field [<Object>]", input = InputType.YES)
public void SetAndTab() {
    if (elementEnabled()) {
        Element.clear();
        Element.sendKeys(Data);
        Element.sendKeys(Keys.TAB);
        Report.updateTestLog(Action, "Entered Text '" + Data + "' on '"
                + ObjectName + "'", Status.DONE);
    } else {
        throw new ElementException(ExceptionType.Element_Not_Enabled, ObjectName);
    }
}

@Action(object = ObjectType.SELENIUM, desc = "Enter the value [<Data>] in the Field [<Object>]", input = InputType.YES)
public void SetOnly() {
    if (elementEnabled()) {
        Element.sendKeys(Data);
        Report.updateTestLog(Action, "Entered Text '" + Data + "' on '"
                + ObjectName + "'", Status.DONE);
    } else {
        throw new ElementException(ExceptionType.Element_Not_Enabled, ObjectName);
    }
}

@Action(object = ObjectType.SELENIUM, desc = "Enter the value [<Data>] in the Field [<Object>]", input = InputType.YES)
public void Upload() {
	try {
		try {

			Element.getAttribute("style").replace("display: none", "display: enable");
			Thread.sleep(200);
			Element.sendKeys(Data);
			Thread.sleep(1000);
			Element.getAttribute("style").replace("display: enable", "display: none");

			Thread.sleep(200);
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Report.updateTestLog(Action, "Entered Text '" + Data + "' on '" + ObjectName + "'", Status.DONE);
	} catch (Exception e) {
	// TODO: handle exception
}
	
	
      
   
   
}

@Action(object = ObjectType.BROWSER, desc = "+1",input = InputType.YES)
public void increment() {
	String strObj = Input;
if (strObj.startsWith("%") && strObj.endsWith("%")) {
	int i=Integer.parseInt(strObj); 
	i=i+1;
	String j=String.valueOf(i);
    addVar(strObj, j);
    Report.updateTestLog(Action, "Current URL '" + j
            + "' is stored in variable '" + strObj + "'", Status.PASS);
} else {
    Report.updateTestLog(Action, "Variable format is not correct", Status.FAIL);
}}

@Action(object = ObjectType.SELENIUM, desc = "Simple sendKeys",input = InputType.YES)
public void simpleSet() {    
        Element.sendKeys(Data);
        Report.updateTestLog(Action, "Entered Text '" + Data + "' on '"
                + ObjectName + "'", Status.DONE);
}
String absolutePath;
@Action(object = ObjectType.ANY, desc = "Fill the project location",input = InputType.YES)
public HashMap<String, String> readXML() { 
	HashMap<String, String> xmlHashMap = null;
	try {
		String projectLocation = Control.getCurrentProject().getLocation();
		 absolutePath = projectLocation + "/XML/" + Input;
		  xmlHashMap = readXMLAsHashMap(absolutePath);
		  System.out.println("test:-"+xmlHashMap);
		  Report.updateTestLog(Action, "Entered Text '" + absolutePath + "' on '"+ ObjectName + "'", Status.DONE);
	} catch (Exception e) {
		Report.updateTestLog(Action, "Entered Text '" + absolutePath + "' on '"+ ObjectName + "'", Status.DEBUG);
	}
	
	        
			return xmlHashMap;
}



public HashMap<String, String> readXMLAsHashMap(String filePath) {
    HashMap<String, String> hashMap = new HashMap<>();

    try {
        // Create a DocumentBuilderFactory
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        // Parse the XML file
        Document document = builder.parse(new File(filePath));

        // Normalize the XML structure
        document.getDocumentElement().normalize();

        // Get the root element
        Element root = document.getDocumentElement();

        // Recursively process child nodes
        processNode(root, hashMap);
    } catch (Exception e) {
        e.printStackTrace();
    }

    return hashMap;
}


private void processNode(Node node, HashMap<String, String> hashMap) {
    // Check if the current node is an element
    if (node.getNodeType() == Node.ELEMENT_NODE) {
        // Get the element name and value
        String name = node.getNodeName();
        String value = node.getTextContent().trim();

        // Add the element to the HashMap
        hashMap.put(name, value);

        // Process child nodes
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            processNode(child, hashMap);
        }
    }
}


@Action(object = ObjectType.DATABASE, desc = "Store XML Element In DataSheet ", input = InputType.YES, condition = InputType.YES)
public void storeXMLelementInDataSheet() {

    try {
        String strObj = Input;
        if (strObj.matches(".*:.*")) {
            try {
                System.out.println("Updating value in SubIteration " + userData.getSubIteration());
                String sheetName = strObj.split(":", 2)[0];
                String columnName = strObj.split(":", 2)[1];
                responsebodies=readXML();
                String xmlText = responsebodies.get(key);
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder;
                InputSource inputSource = new InputSource();
                inputSource.setCharacterStream(new StringReader(xmlText));
                dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(inputSource);
                doc.getDocumentElement().normalize();
                XPath xPath = XPathFactory.newInstance().newXPath();
                String expression = Condition;
                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
                Node nNode = nodeList.item(0);
                String value = nNode.getNodeValue();
                userData.putData(sheetName, columnName, value);
                Report.updateTestLog(Action, "Element text [" + value + "] is stored in " + strObj, Status.DONE);
            } catch (IOException | ParserConfigurationException | XPathExpressionException | DOMException
                    | SAXException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.OFF, ex.getMessage(), ex);
                Report.updateTestLog(Action, "Error Storing XML element in datasheet :" + "\n" + ex.getMessage(),
                        Status.DEBUG);
            }
        } else {
            Report.updateTestLog(Action,
                    "Given input [" + Input + "] format is invalid. It should be [sheetName:ColumnName]",
                    Status.DEBUG);
        }
    } catch (Exception ex) {
        Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
        Report.updateTestLog(Action, "Error Storing XML element in datasheet :" + "\n" + ex.getMessage(),
                Status.DEBUG);
    }
}








}