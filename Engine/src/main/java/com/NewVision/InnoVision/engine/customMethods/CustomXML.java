package com.NewVision.InnoVision.engine.customMethods;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.NewVision.InnoVision.engine.commands.General;
import com.NewVision.InnoVision.engine.core.CommandControl;
import com.NewVision.InnoVision.engine.core.Control;
import com.NewVision.InnoVision.engine.support.Status;
import com.NewVision.InnoVision.engine.support.methodInf.Action;
import com.NewVision.InnoVision.engine.support.methodInf.InputType;
import com.NewVision.InnoVision.engine.support.methodInf.ObjectType;


public class CustomXML extends General {
	static public Map<String, String> xml = new HashMap<>();
    public CustomXML(CommandControl cc) {
		super(cc);
	}
    
    String XMLFILENAME = getVar("%XMLFileName%");
    static List<String> colNames = new ArrayList<>();


@Action(object = ObjectType.DATABASE, desc = "Store XML Element In DataSheet ", input = InputType.YES, condition = InputType.YES )
public void storeXMLintoDataSheet() {
    try {
        String strObj = Input;
        if (strObj.matches(".*:.*")) {
            try {
            	    		    
                System.out.println("Updating value in SubIteration " + userData.getSubIteration());
                String sheetName = strObj.split(":", 2)[0];
                String columnName = strObj.split(":", 2)[1];
                String projectLocation = Control.getCurrentProject().getLocation();
       		    String absolutePath = projectLocation + "/XML/" + XMLFILENAME;
       		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(absolutePath);
                document.getDocumentElement().normalize();
                HashMap<String, String> hashMap = new HashMap<>();
                xml=hashMap;
                String xmlText = xml.get(key);
                System.out.println("String :- "+xmlText);
                DocumentBuilder dBuilder;
                InputSource inputSource = new InputSource();
                inputSource.setCharacterStream(new StringReader(xmlText));
                dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(inputSource);
                doc.getDocumentElement().normalize();
                XPath xPath = XPathFactory.newInstance().newXPath();
                String expression = Condition;
                Logger.getLogger(Condition);
                System.out.println(Condition);
                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
                Node nNode = nodeList.item(0);
                String value = nNode.getNodeValue();
                userData.putData(sheetName, columnName, value);
                Report.updateTestLog(Action, "Element text [" + value + "] is stored in " + strObj, Status.DONE);
            } catch (IOException | ParserConfigurationException | XPathExpressionException | DOMException
                    | SAXException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.OFF, ex.getMessage(), ex);
                Report.updateTestLog(Action, "Error Storing XML element in datasheet :" + "\n" + ex.getMessage(),
                        Status.DEBUG);
            }
        } else {
            Report.updateTestLog(Action,
                    "Given input [" + Input + "] format is invalid. It should be [sheetName:ColumnName]",
                    Status.DEBUG);
        }
    } catch (Exception ex) {
        Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
        Report.updateTestLog(Action, "Error Storing XML element in datasheet :" + "\n" + ex.getMessage(),
                Status.DEBUG);
    }
}
public String xml(){
	return xml.get(key);
}

@Action(object = ObjectType.DATABASE, desc = "Store XML Element In DataSheet ", input = InputType.YES, condition = InputType.YES )
public void storeXMLValueintoDataSheet() {
	    try {
        String strObj = Input;
        if (strObj.matches(".*:.*")) {
            try {
            	// Create a DocumentBuilderFactory
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  		    
                System.out.println("Updating value in SubIteration " + userData.getSubIteration());
                String sheetName = strObj.split(":", 2)[0];
                String columnName = strObj.split(":", 2)[1];
                String projectLocation = Control.getCurrentProject().getLocation();
       		    String absolutePath = projectLocation + "/XML/" + XMLFILENAME;
       		    // Create a DocumentBuilder
                Document doc = factory.newDocumentBuilder().parse(absolutePath);
                // Create an XPathFactory
                XPathFactory xPathFactory = XPathFactory.newInstance();
                // Create an XPath object
                XPath xpath = xPathFactory.newXPath();
                // Example XPath expressions
                String expression = Condition;
                // Execute XPath expressions
                NodeList nodeList = (NodeList) xpath.evaluate(expression, doc, XPathConstants.NODESET);
                Logger.getLogger(Condition);
                System.out.println(Condition);
               String value= nodeList.item(0).getTextContent();
                userData.putData(sheetName, columnName, value);
                Report.updateTestLog(Action, "Element text [" + value + "] is stored in " + strObj, Status.DONE);
            } catch (IOException | ParserConfigurationException | XPathExpressionException | DOMException
                    | SAXException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.OFF, ex.getMessage(), ex);
                Report.updateTestLog(Action, "Error Storing XML element in datasheet :" + "\n" + ex.getMessage(),
                        Status.DEBUG);
            }
        } else {
            Report.updateTestLog(Action,
                    "Given input [" + Input + "] format is invalid. It should be [sheetName:ColumnName]",
                    Status.DEBUG);
        }
    } catch (Exception ex) {
        Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
        Report.updateTestLog(Action, "Error Storing XML element in datasheet :" + "\n" + ex.getMessage(),
                Status.DEBUG);
    }
}

@Action(object = ObjectType.DATABASE, desc = "Store XML Element In DataSheet ", input = InputType.YES, condition = InputType.YES )
public void storeXMLNameintoDataSheet() {
	    try {
        String strObj = Input;
        if (strObj.matches(".*:.*")) {
            try {
            	// Create a DocumentBuilderFactory
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  		    
                System.out.println("Updating value in SubIteration " + userData.getSubIteration());
                String sheetName = strObj.split(":", 2)[0];
                String columnName = strObj.split(":", 2)[1];
                String projectLocation = Control.getCurrentProject().getLocation();
       		    String absolutePath = projectLocation + "/XML/" + XMLFILENAME;
       		    // Create a DocumentBuilder
                Document doc = factory.newDocumentBuilder().parse(absolutePath);
                // Create an XPathFactory
                XPathFactory xPathFactory = XPathFactory.newInstance();
                // Create an XPath object
                XPath xpath = xPathFactory.newXPath();
                // Example XPath expressions
                String expression = Condition;
                // Execute XPath expressions
                NodeList nodeList = (NodeList) xpath.evaluate(expression, doc, XPathConstants.NODESET);
                Logger.getLogger(Condition);
                System.out.println(Condition);
               String value= nodeList.item(0).getNodeName();
                userData.putData(sheetName, columnName, value);
                Report.updateTestLog(Action, "Element text [" + value + "] is stored in " + strObj, Status.DONE);
            } catch (IOException | ParserConfigurationException | XPathExpressionException | DOMException
                    | SAXException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.OFF, ex.getMessage(), ex);
                Report.updateTestLog(Action, "Error Storing XML element in datasheet :" + "\n" + ex.getMessage(),
                        Status.DEBUG);
            }
        } else {
            Report.updateTestLog(Action,
                    "Given input [" + Input + "] format is invalid. It should be [sheetName:ColumnName]",
                    Status.DEBUG);
        }
    } catch (Exception ex) {
        Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
        Report.updateTestLog(Action, "Error Storing XML element in datasheet :" + "\n" + ex.getMessage(),
                Status.DEBUG);
    }
}

@Action(object = ObjectType.DATABASE, desc = "Store XML Element In DataSheet ", input = InputType.NO, condition = InputType.YES )
public void fileNamefromFolder() {
	String reuslt = "";
	try {
		String projectLocation = Control.getCurrentProject().getLocation();
		    String sourceLocation = projectLocation + "/XML/";
		File folder = new File(sourceLocation);
        if (folder.isDirectory()) {
            File[] files = folder.listFiles();
            int totalRows = files.length;
            int totalCols = 1;
            
            for (int colIndex = 0; colIndex < totalCols; colIndex++) {
            if (files != null) {
                for (File file : files) {
                    if (file.isFile()) {
                    	reuslt= file.getName();
                        System.out.println(file.getName());
                    }
                }
                for (int rowIndex = 1; rowIndex <= totalRows; rowIndex++) {
                    if (files != null) {
                    	userData.putData(sourceLocation, sourceLocation, reuslt, projectLocation, sourceLocation);
                        userData.putData(Condition, colNames.get(colIndex), reuslt, userData.getIteration(), Integer.toString(rowIndex));
                    } else {
                        Report.updateTestLog(Action, "Row " + rowIndex + " doesn't exist",
                                Status.FAILNS);
                        return;
                    }
                }
                }
            }
            
        }
	  
     
    
    Report.updateTestLog(Action, "  Query Result has been saved in DataSheet: ",
            Status.PASSNS);
    
} catch (Exception ex) {
    Report.updateTestLog(Action, "Error executing the  Query: " + ex.getMessage(),
            Status.FAILNS);
}
}




}